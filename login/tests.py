from django.test import TestCase, Client
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class UnittestStory9(TestCase) :
    def test_url_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_url_proses(self):
        response = Client().get('/loginsuccess/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'proses.html')

    def test_button_dilogin(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Login", content)

    def test_button_dilogout(self):
        c = Client()
        response = c.get('/loginsuccess/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Logout", content)

